from django.forms import ModelForm

from posts.models import Post

# this class will handle all of 
class PostForm(ModelForm):
    class Meta: 
        model = Post
        fields = "__all__"
from django.shortcuts import render, redirect

# import Post model to get posts from the database
from posts.models import Post
from posts.forms import PostForm

# Create your views here.

def list_posts(request):
    posts = Post.objects.all()
    context = {
        "posts" : posts,
    }
    return render(request, "posts/main.html", context)


# from posts.forms declare a function named create_post which has a 
# single input parameter that contains the HTTP request
def create_post(request):
    # create a new post form for the request
    form = PostForm()
    # if the request is a Post request then
    if request.method == "POST":
        # create a new form with the data in the HTTP Post
        form = PostForm(request.POST)
        # test to see if the data is valid
        if form.is_valid():
            # since the data is valid, save the data
            form.save()
            # redirect to the main page that contains the list of posts
            return redirect("posts_list")
    # create a context dictionary
    context = {
        # add the form to the dictionary
        "form" : form, 
    }
    # render the template and return that to Django
    return render(request, "posts/create.html", context)


# the post_detail function gets the id of the post that we want to show
# and shows it
def post_detail(request, id):
    post = Post.objects.get(id=id)
    context = {
        "post" : post,
    }
    return render(request, "posts/detail.html", context)
